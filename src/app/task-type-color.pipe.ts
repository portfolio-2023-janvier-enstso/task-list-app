import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'taskTypeColor'
})
export class TaskTypeColorPipe implements PipeTransform {

  transform(status: string): string {
     let color : string;
    
     switch(status){
      case 'fait':
        color = "blue";
        break;
      case 'à faire':
        color = "grey";
        break;
     }
     console.log(color);
     return 'chip-' + color;
  }

  

}
