export class task{
    id!: number;
    text!: string;
    order!: number;
    date!: Date;
    status!: string;
}