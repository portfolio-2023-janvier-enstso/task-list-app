import { Directive, ElementRef, HostListener, Input } from '@angular/core';

@Directive({
  selector: '[appBorderCard]'
})
export class BorderCardDirective {

  initialColor = 'grey';
  eventColor = 'black';
  initialHeight = 180

  constructor(private elmt: ElementRef) { 
    this.setBorder(this.initialColor);
    this.setHeight(this.initialHeight);
  }

  @Input('appBorderCard') borderColor: string
  
  @HostListener('mouseenter') onMouseEnter(){
    this.setBorder(this.borderColor);
  }

  @HostListener('mouseleave') onMouseLeave(){
    this.setBorder(this.initialColor);

  }
  setHeight(height : number){
    this.elmt.nativeElement.style.height = `${height}px`;
  }

  setBorder(color: string){
    this.elmt.nativeElement.style.border = `solid 3px ${color}`;
    console.log(  this.elmt.nativeElement.style.color)
  }

}
